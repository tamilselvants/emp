<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    
});

Route::resource('sample','CrudController@create');

Auth::routes();


Route::get('insert','Student@insertform');
Route::post('create','Student@insert');
Route::get('view-records','Student@index');

Route::get('delete-records','Student@delete');
Route::get('delete/{id}','Student@destroy');

Route::get('retrieve','Student@retrieve');

Route::get('update','Student@update');
Route::get('edit/{id}','Student@show');
Route::post('edit/{id}','Student@edit');

