<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Student extends Controller
{
    //form for getting input
    public function insertform(){
        try{
        return view('stud_create');
        }catch (Exception $ex) {
            Log::debug($ex->getMessage());
        }
    }

    //Insert record in database 
    //@param-string
    public function insert(Request $request){
        try{
        $id = $request->input('id');
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $city_name = $request->input('city_name');
        $email = $request->input('email');
        $data=array('id'=>$id,'first_name'=>$first_name,"last_name"=>$last_name,"city_name"=>$city_name,"email"=>$email);
        DB::table('student')->insert($data);
        echo "Record inserted successfully.<br/>";
    }catch (Exception $ex) {
        Log::debug($ex->getMessage());
    }
    }

    //to display all records
    public function index(){
        try{
        $users = DB::select('select * from student');
        return view('stud_view',['users'=>$users]);
    }catch (Exception $ex) {
        Log::debug($ex->getMessage());
    }
    }

    //to delete a record from table
    //@param-string
    public function destroy($id) {
        try{
        DB::delete('delete from student where id = ?',[$id]);
        echo "Record deleted successfully.";
    }catch (Exception $ex) {
        Log::debug($ex->getMessage());
    }
    }


    public function delete(){
        $users = DB::select('select * from student');
        return view('stud_delete_view',['users'=>$users]);
        }


    public function retrieve(){
            $users = DB::select('select * from student');
            return view('stud_view',['users'=>$users]);
            }


    public function update(){
            $users = DB::select('select * from student');
            return view('stud_edit_view',['users'=>$users]);
            }
    //to display the particular record
    public function show($id) {
        try{
    $users = DB::select('select * from student where id = ?',[$id]);
    return view('stud_update',['users'=>$users]);
        }catch (Exception $ex) {
            Log::debug($ex->getMessage());
        }
    }

    //to edit records
    //@param-string
    public function edit(Request $request,$id) {
        try{
    $first_name = $request->first_name;
    $last_name = $request->last_name;
    $city_name = $request->city_name;
    $email = $request->email;
    
    DB::update('update student set first_name = ?,last_name=?,city_name=?,email=? where id = ?',[$first_name,$last_name,$city_name,$email,$id]);
    echo "Record updated successfully";
    }catch (Exception $ex) {
        Log::debug($ex->getMessage());
    }
    }
}